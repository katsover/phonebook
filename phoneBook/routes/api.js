// initialize our faux database
var data = {
  "contacts": [
   {'name': 'Name 1',
     'phone': '09123456789'},
    {'name': 'Name 2',
     'phone': '09865432131'},
    {'name': 'Name 3',
     'phone': '03213596777'}
  ]
};

// GET

exports.contacts = function (req, res) {
  var contacts = [];
  data.contacts.forEach(function (contact, i) {
    contacts.push({
      id: i,
      name: contact.name,
      phone: contact.phone
    });
  });
  res.json({
    contacts: contacts
  });
};

exports.contact = function (req, res) {
  var id = req.params.id;

  if (id >= 0 && id < data.contacts.length) {
    res.json({
      contact: data.contacts[id]
    });
  } else {
    res.json(false);
  }
};

// POST

exports.addContact = function (req, res) {
  data.contacts.push(req.body);
  res.json(req.body);
};

// PUT

exports.editContact = function (req, res) {
  var id = req.params.id;

  if (id >= 0 && id < data.contacts.length) {
    data.contacts[id] = req.body;
    res.json(true);
  } else {
    res.json(false);
  }
};

// DELETE

exports.deleteContact = function (req, res) {
  var id = req.params.id;
  if (id >= 0 && id < data.contacts.length) {
    data.contacts.splice(id, 1);
    res.json(true);
  } else {
    res.json(false);
  }
};