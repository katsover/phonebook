var contactApp = angular.module('contactApp', ['ui.router']);


contactApp.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/contacts");

  $stateProvider.state('contacts', { // state for showing all contacts
    url: '/contacts',
    views:{
      'content' : {
        templateUrl: 'partials/contacts',
        controller: 'ContactListController'
      }
    }   
  }).state('viewContact', { //state for showing single contact
    url: '/contacts/:id/view',
    views:{
      'modal' : {
        templateUrl: 'partials/contact-view',
        controller: 'ContactViewController'
      },
      'content' : {
        templateUrl: 'partials/contacts',
        controller: 'ContactListController'
      }
    }    
  }).state('newContact', { //state for adding a new contact
    url: '/contacts/addContact',
    views:{
      'modal' : {
        templateUrl: 'partials/contact-add',
        controller: 'ContactCreateController'
      },
      'content' : {
        templateUrl: 'partials/contacts',
        controller: 'ContactListController'
      }
    }  
  }).state('editContact', { //state for updating a contact
    url: '/contacts/:id/edit',
    
     views:{
      'modal' : {
        templateUrl: 'partials/contact-edit',
        controller: 'ContactEditController'
      },
      'content' : {
        templateUrl: 'partials/contacts',
        controller: 'ContactListController'
      }
    }  
  }).state('deleteContact', { //state for updating a contact
    url: '/contacts/:id/delete',
    
     views:{
      'modal' : {
        templateUrl: 'partials/contact-delete',
        controller: 'ContactDeleteController'
      },
      'content' : {
        templateUrl: 'partials/contacts',
        controller: 'ContactListController'
      }
    }  
  });
}]);

