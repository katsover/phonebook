function ContactListController($scope, $http) {

  $http.get('/api/contacts').
    success(function(data, status, headers, config) {
      $scope.contacts = data.contacts;
    });
}

function ContactCreateController($scope, $http, $location) {
  $scope.form = {};

  $scope.submitPost = function () {
    $http.post('/api/contact', $scope.form).
      success(function(data) {
        $("#myModal").modal('hide');
         $location.url('/' );
      });
  };
}

function ContactViewController($scope, $http, $stateParams) {

  $http.get('/api/contact/' + $stateParams.id).
    success(function(data) {

      $scope.contact = data.contact;
      $scope.id = $stateParams.id;
    });
}

function ContactEditController($scope, $http, $location, $stateParams) {
  $scope.form = {};
  $http.get('/api/contact/' + $stateParams.id).
    success(function(data) {
      $scope.form = data.contact;
    });

  $scope.editContact = function () {
    $http.put('/api/contact/' + $stateParams.id, $scope.form).
      success(function(data) {
         $("#myModal").modal('hide');
        $location.url('/');
      });
  };
}

function ContactDeleteController($scope, $http, $location, $stateParams) {

  $http.get('/api/contact/' + $stateParams.id).
    success(function(data) {
      $scope.contact = data.contact;
    });

  $scope.deleteContact = function () {
    $http.delete('/api/contact/' + $stateParams.id).
      success(function(data) {
        $("#myModal").modal('hide');
        $location.url('/');
      });
  };

  $scope.home = function () {
      $("#myModal").modal('hide');
      $location.url('/');
  };
}