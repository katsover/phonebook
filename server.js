var express = require('express'),
    stylus = require('gulp-stylus'),
    api = require('./phoneBook/routes/api'),
    routes = require('./phoneBook/routes'),
    bodyParser = require('body-parser');

var app = module.exports = express();

app.set('views', __dirname + '/phoneBook/views')
app.set('view engine', 'jade')

app.use( bodyParser.json() );      
app.use(bodyParser.urlencoded({ 
  extended: true
})); 

app.use(express.static(__dirname + '/phoneBook'));

// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);


app.get('/api/contacts', api.contacts);

app.get('/api/contact/:id', api.contact);
app.post('/api/contact', api.addContact);
app.put('/api/contact/:id', api.editContact);
app.delete('/api/contact/:id', api.deleteContact);

app.listen(3000)